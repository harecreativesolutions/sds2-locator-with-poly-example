from Component import RegisterComponentType
from component_tools import RegisterComponentAddCommand
from LocatorSample import LocatorSample

RegisterComponentType(LocatorSample, LocatorSample.UserStr)
RegisterComponentAddCommand(LocatorSample)