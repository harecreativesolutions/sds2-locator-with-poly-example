import copy
import math

from Designable.Proxies import RectPlate
from Layout3D import Layout3D
from Transform3D import Transform3D
from job import Job
from sds2 import obj

def design_material(self):
    atts = {
        'Member': self.member_number,
        'Point1': self.Point1,
        'Point2': self.Point2,
        'MaterialGrade': Job().steel_grades("Plate").keys()[0],
        'Width': self.Width,
        'Thickness': self.Thickness,
        'MaterialOriginPoint': "Center",
        'MaterialType': "Plate",
        'WorkpointSlopeDistance': self.Point1.Distance(self.Point2)
    }

    proxy = RectPlate(**atts)

    translation = copy.copy(self.Point1)
    self.HostXform.TransformPoint(translation)

    xform = copy.copy(self.HostXform)  # don't want to modify the original
    xform.SetTranslation(translation)

    proxy.SetTransform(xform)

    return proxy
