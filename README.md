# LocatorSample

A sample custom component which utilizes the Locator3D API endpoint to restrict locator choice and generate a dynamic preview.

Please feel free to ask questions or report any issues. 